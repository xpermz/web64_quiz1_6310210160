import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";


import { createTheme ,ThemeProvider} from '@mui/material/styles';
import { green, purple } from '@mui/material/colors';



const theme = createTheme({
  palette: {
    type: 'light',
    primary: {
      main: '#d01717',
    },
    secondary: {
      main: '#d02626',
    },
    text: {
      primary: 'rgba(243,0,114,0.87)',
      secondary: 'rgba(199,11,11,0.54)',
      disabled: 'rgba(214,18,18,0.38)',
      hint: 'rgba(166,23,23,0.38)',
    },
  },
});


ReactDOM.render(
  <BrowserRouter>
  <ThemeProvider theme={theme}>
    <App />
  </ThemeProvider>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
