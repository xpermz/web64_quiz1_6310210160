import logo from './logo.svg';
import './App.css';
import AboutUs from './components/AboutUs';
import Header from './components/Header';
import Calculate from './components/Calculate';

import {Routes , Route } from "react-router-dom";

function App() {
  return (
    <div>
      <Header/>
      <Routes>
        <Route path="About" element ={<AboutUs/>}/>
        <Route path="/" element ={<Calculate/>}/>

      </Routes>
      
      
    </div>
  );
}

export default App;
