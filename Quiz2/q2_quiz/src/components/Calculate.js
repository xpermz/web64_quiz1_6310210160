import {useState} from "react";
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

function Calculate(){

    const[score,setScore] =useState("");
    const[grade,setGrade] =useState("");

    function calculateGrade(){
        let sc = parseInt(score)


        if(sc<0 || sc >100){
            setGrade("Out of range!! (0-100)");
        }else{
            if(sc>=80){
                setGrade("A");
            }else if(sc>=75){
                setGrade("B+");
            }else if(sc>=70){
                setGrade("B");
            }else if(sc>=65){
                setGrade("C+");
            }else if(sc>=60){
                setGrade("C");
            }else if(sc>=55){
                setGrade("D+");
            }else if(sc>=50){
                setGrade("D");
            }else{
                setGrade("F");
            }
        }


    }
    return(
        <div>
            


            
            <TextField id="outlined-basic" label="Please enter you score" variant="outlined" 
            value = {score} onChange={(e)=>{setScore(e.target.value);}} />
            <br/>
            <Button onClick = {() => {calculateGrade()}} variant="contained">CALCULATE</Button>
           
            <br/>
            <br/>
            
            <h1>your score is : {grade} </h1>
            <br/>
            

            
            
        </div>
    );
}

export default Calculate;